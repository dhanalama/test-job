<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;


Auth::routes(['verify' => true]);

Route::get('/', 						'PostController@showDashboard')->name('home');
Route::get('/{slug}', 				'PostController@getDetailBySlug')->name('post.detail');

Route::middleware('auth')->group(function () {
	Route::get('/post/create', 			'PostController@createPost')->name('post.create');
	Route::post('/post/store', 			'PostController@storePost')->name('post.store');
	Route::post('/post/{id}/comment', 	'PostController@storeComment')->name('post.comment');
	
	Route::get('/user/{id}/edit', 		'UserController@editProfile')->name('user.edit');
	Route::post('/user/{id}/update', 	'UserController@updateProfile')->name('user.update');
});

