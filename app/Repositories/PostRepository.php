<?php


namespace App\Repositories;


interface PostRepository
{
	
	/**
	 * Get Random Posts
	 *
	 * @param int $limit
	 * @return mixed
	 */
	public function getRandomPosts(int $limit);
	
	
	/**
	 * Find Post By Slug
	 *
	 * @param $slug
	 * @return mixed
	 */
	public function findBySlug($slug);
	
	
	
	/**
	 * Get Post Details By Id
	 *
	 * @param $id
	 * @return mixed
	 */
	public function getDetailsById($id);
	
	
	/**
	 * Store Comment
	 *
	 * @param $comment
	 * @param $postId
	 * @param $userId
	 * @return mixed
	 */
	public function storeComment($comment, $postId, $userId);
	
	
	/**
	 * Store Post
	 *
	 * @param $userId
	 * @param $title
	 * @param $description
	 * @return mixed
	 */
	public function storePost($userId, $title, $description);
	
	
}