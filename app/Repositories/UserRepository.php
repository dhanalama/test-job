<?php


namespace App\Repositories;


interface UserRepository
{
	
	/**
	 * Find User By Id
	 *
	 * @param int $userId
	 * @return mixed
	 */
	public function findById(int $userId);
	
	
	
	/**
	 * Update User
	 *
	 * @param $userId
	 * @param $firstName
	 * @param $lastName
	 * @param $userName
	 * @param $email
	 * @param $password
	 * @param $phoneNo
	 * @param $address
	 * @return mixed
	 */
	public function updateUser($userId, $firstName, $lastName, $userName, $email, $password, $phoneNo, $address);
}