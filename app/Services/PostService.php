<?php


namespace App\Services;


use App\Models\Comment;
use App\Models\Post;
use App\Repositories\PostRepository;

class PostService implements PostRepository
{
	private $model;

	public function __construct(Post $model)
	{
		$this->model = $model;
	}
	
	/**
	 * Get Random Posts
	 *
	 * @param int $limit
	 * @return mixed
	 */
	public function getRandomPosts(int $limit)
	{
		return $this->model->select(
			'posts.id as id', 'posts.title as title', 'posts.slug as slug',
			'posts.description as description', 'posts.created_at as created_at',
			'users.name as author'
			)
			->join('users', 'posts.user_id', 'users.id')
			->inRandomOrder()
			->limit($limit)
			->get();
	}
	
	
	/**
	 * Find Post By Slug
	 *
	 * @param $slug
	 * @return mixed
	 */
	public function findBySlug($slug)
	{
		return $this->model->filterBySlug($slug)->first();
	}
	
	/**
	 * Get Post Details By Id
	 *
	 * @param $id
	 * @return mixed
	 */
	public function getDetailsById($id)
	{
		return $this->model->select(
			'posts.id as id', 'posts.title as title', 'posts.slug as slug',
			'posts.description as description', 'posts.created_at as created_at',
			'users.name as author'
		)
			->with(['commentators'])
			->join('users', 	'posts.user_id', 	'users.id')
			->where('posts.id', $id)
			->first();
	}
	
	
	/**
	 * Store Comment
	 *
	 * @param $comment
	 * @param $postId
	 * @param $userId
	 * @return mixed
	 */
	public function storeComment($comment, $postId, $userId)
	{
		return Comment::create([
			'user_id' 	=> $userId,
			'post_id' 	=> $postId,
			'text'		=> $comment
		]);
	}
	
	/**
	 * Store Post
	 *
	 * @param $userId
	 * @param $title
	 * @param $description
	 * @return mixed
	 */
	public function storePost($userId, $title, $description)
	{
		return $this->model->create([
			'user_id' 	=> $userId,
			'title'		=> $title,
			'slug'		=> $title,
			'description' => $description
		]);
	}
}