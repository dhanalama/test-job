<?php


namespace App\Services;


use App\Repositories\UserRepository;
use App\User;

class UserService implements UserRepository
{
	private $model;

	public function __construct(User $model)
	{
		$this->model = $model;
	}
	

	/**
	 * Find By User Id
	 *
	 * @param int $userId
	 * @return mixed
	 */
	public function findById(int $userId)
	{
		return $this->model->find($userId);
	}
	
	/**
	 * Update User
	 *
	 * @param $userId
	 * @param $firstName
	 * @param $lastName
	 * @param $userName
	 * @param $email
	 * @param $password
	 * @param $phoneNo
	 * @param $address
	 * @return mixed
	 */
	public function updateUser($userId, $firstName, $lastName, $userName, $email, $password, $phoneNo, $address)
	{
		$user = $this->model->find($userId);
		
		$user->first_name 	= $firstName;
		$user->last_name 	= $lastName;
		$user->name 		= $userName;
		$user->email 		= $email;
		if (!empty($password)) {
			$user->password = $password;
		}
		$user->phone_no 	= $phoneNo;
		$user->address 		= $address;
		$user->save();
	}
}