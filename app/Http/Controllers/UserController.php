<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserValidator;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;


class UserController extends Controller
{
	/**
	 * @var PostRepository
	 */
	private $userRepository;
	
	/**
	 * Create a new controller instance.
	 *
	 * @param UserRepository $userRepository
	 */
    public function __construct(UserRepository $userRepository)
    {
		$this->userRepository = $userRepository;
	}
	
	/**
	 * Edit User Information
	 *
	 * @param $userId
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function editProfile($userId)
    {
    	$user = $this->userRepository->findById($userId);
    	
    	if (empty($user))
    		abort(404);
    	
		return view('user.edit', compact('user'));
    }
	
	/**
	 * Update User Information
	 *
	 * @param UpdateUserValidator $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function updateProfile(UpdateUserValidator $request)
	{
		$this->userRepository->updateUser(auth()->id(),
			$request->get('first_name'),
			$request->get('last_name'),
			$request->get('name'),
			$request->get('email'),
			$request->get('password'),
			$request->get('phone_no'),
			$request->get('address')
		);
		
		flash('Profile Updated Successfully')->success();
		
		return redirect()->back();
    }
}
