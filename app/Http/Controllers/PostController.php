<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreComment;
use App\Http\Requests\StorePostValidator;
use App\Repositories\PostRepository;

class PostController extends Controller
{
	/**
	 * @var PostRepository
	 */
	private $postRepository;
	
	/**
	 * Create a new controller instance.
	 *
	 * @param PostRepository $postRepository
	 */
    public function __construct(PostRepository $postRepository)
    {
		$this->postRepository = $postRepository;
	}
	
	
	/**
	 * Dashboard Random Posts
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function showDashboard()
    {
    	$posts = $this->postRepository->getRandomPosts(12);
    	
		return view('home', compact('posts'));
    }
	
	
	/**
	 * Post Details By Slug
	 *
	 * @param $slug
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getDetailBySlug($slug)
	{
		$post = $this->postRepository->findBySlug($slug);
		
		if (empty($post))
			abort(404);
		
		$postDetail = $this->postRepository->getDetailsById($post->id);
		
		return view('post.detail', compact('postDetail'));
	}
	
	
	/**
	 * Store Comment
	 *
	 * @param StoreComment $request
	 * @param $postId
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function storeComment(StoreComment $request, $postId)
	{
		$this->postRepository->storeComment($request->get('comment'), $postId, auth()->id());
		
		flash('Commented Successfully')->success();
		
		return redirect()->back();
	}
	
	
	/**
	 * New Post Form
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function createPost()
	{
		return view('post.create');
	}
	
	
	/**
	 * Store Post
	 *
	 * @param StorePostValidator $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function storePost(StorePostValidator $request)
	{
		$post = $this->postRepository->storePost(
			auth()->id(),
			$request->get('title'),
			$request->get('description')
		);
		
		flash('New Post created Successfully')->success();
		
		return redirect()->route('post.detail', ['slug' => $post->slug]);
	}
}
