<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostValidator extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' 		=> 'required|min:4',
			'description' 	=> 'required|min:12',
		];
	}
	
	
	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'title.required' 		=> 'Title is required',
			'title.min' 			=> 'Title must be at least 4 letters long',
			'description.required' 	=> 'Description is required',
			'description.min' 		=> 'Description must be at least 12 letters long',
		];
	}
}
