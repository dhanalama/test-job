<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserValidator extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'first_name' 	=> 'required|min:2',
			'last_name' 	=> 'required|min:2',
			'name' 			=> 'required|min:2',
			'phone_no' 		=> 'required|min:10|numeric',
			'email'			=> 'required|'.Rule::unique('users')->ignore(auth()->id()),
			'password'		=> 'nullable|min:6|string',
			'address'		=> 'required|min:8'
		];
	}
	
	
	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'first_name.required' 	=> 'First Name is required',
			'first_name.min' 		=> 'First Name must be at least 2 letters long',
			'last_name.required' 	=> 'Last Name is required',
			'last_name.min' 		=> 'Last Name must be at least 2 letters long',
			'name.required' 		=> 'UserName is required',
			'name.min' 				=> 'UserName must be at least 2 letters long',
			'phone_no.required' 	=> 'Phone No is required',
			'phone_no.min' 			=> 'Phone No must be at least 10 digits long',
			'phone_no.numeric' 		=> 'Phone No must be numeric',
			'email.required' 		=> 'Email is required',
			'email.unique' 			=> 'Email must be unique',
			'password.required' 	=> 'Password must be at least 6 characters long',
			'password.string' 		=> 'Password must be string',
			'address.required' 		=> 'Address is required',
			'address.alpha_num' 	=> 'Address must be at least 8 characters long',
		];
	}
}
