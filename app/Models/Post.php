<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';
	
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'title', 'slug', 'description'];
	
	
	
	/**
	 * Setter Slug Attribute
	 *
	 * @param $value
	 */
	public function setSlugAttribute($value)
	{
		$this->attributes['slug'] = str_slug($value);
	}
	
	
	
	
	/**
	 * Filter By Slug
	 *
	 * @param $query
	 * @param $slug
	 * @return mixed
	 */
	public function scopeFilterBySlug($query, $slug)
	{
		return $query->where('slug', $slug);
	}
	

	
	/**
	 * Relationship with Users
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function commentators()
	{
		return $this->belongsToMany(User::class, 'comments', 'post_id', 'user_id')
			->as('comment')
			->withPivot([
				'user_id', 'post_id', 'text'
			])
			->withTimestamps();
	}
	
	
	/**
	 * Relationship with Comments
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function comments()
	{
		return $this->hasMany(Comment::class, 'post_id', 'id');
	}
}
