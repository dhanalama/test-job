@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @include('flash::message')

                @auth
                <div class="clearfix mt-3">
                    <div class="float-right">
                        <a href="{{ route('post.create') }}" class="btn btn-primary">
                            Create Post
                        </a>
                    </div>
                </div>
                @endauth

                <div class="card mt-3">
                    <div class="card-header">Blog Posts</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        @if(!$posts->isEmpty())
                            @foreach($posts as $post)
                                <div class="post-preview">
                                    <a href="{{ route('post.detail', ['slug' => $post->slug]) }}">
                                        <h2 class="post-title">
                                            {{ $post->title }}
                                        </h2>
                                    </a>
                                    <p class="post-meta">Posted by
                                        <strong>{{ $post->author }}</strong>
                                        ({{ $post->created_at->diffForHumans() }})
                                    </p>
                                </div>
                                <hr/>
                            @endforeach
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
