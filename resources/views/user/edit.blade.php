@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @include('flash::message')

                <div class="card mt-4">
                    <div class="card-header">User Profile Edit</div>

                    <div class="card-body">

                        @include('errors.validation')

                        <form action="{{ route('user.update', ['id' => $user->id]) }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group row">
                                <label for="first_name" class="col-sm-2 col-form-label">First Name</label>
                                <div class="col-sm-10">
                                    <input type="text"
                                           name="first_name"
                                           class="form-control"
                                           id="first_name"
                                           value="{{ old('first_name') ?: $user->first_name }}"
                                           placeholder="Enter First Name"
                                    >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="last_name" class="col-sm-2 col-form-label">Last Name</label>
                                <div class="col-sm-10">
                                    <input type="text"
                                           name="last_name"
                                           class="form-control"
                                           id="last_name"
                                           value="{{ old('last_name') ?: $user->last_name }}"
                                           placeholder="Enter Last Name"
                                    >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">UserName</label>
                                <div class="col-sm-10">
                                    <input type="text"
                                           name="name"
                                           class="form-control"
                                           id="name"
                                           value="{{ old('name') ?: $user->name }}"
                                           placeholder="Enter UserName"
                                    >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone_no" class="col-sm-2 col-form-label">Phone No</label>
                                <div class="col-sm-10">
                                    <input type="text"
                                           name="phone_no"
                                           class="form-control"
                                           id="phone_no"
                                           value="{{ old('phone_no') ?: $user->phone_no }}"
                                           placeholder="Enter Phone No"
                                    >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text"
                                           name="email"
                                           class="form-control"
                                           id="email"
                                           value="{{ old('email') ?: $user->email }}"
                                           placeholder="Enter Email Address"
                                    >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-sm-2 col-form-label">New Password</label>
                                <div class="col-sm-10">
                                    <input type="password"
                                           name="password"
                                           class="form-control"
                                           id="password"
                                           placeholder="Enter New Password"
                                    >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                    <textarea type="text"
                                           name="address"
                                           class="form-control"
                                           id="address"
                                    >{{ old('address') ?: $user->address }}</textarea>
                                </div>
                            </div>
                            <div class="clearfix">
                                <button class="btn float-right btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
