@if(!$postDetail->commentators->isEmpty())
    <div class="comment-section mt-5">
        <h4>Comments</h4>
        <ul class="list-group">
            @foreach($postDetail->commentators as $commentator)
                <li class="list-group-item">
                    <p class="float-left">
                        {{ $commentator->comment->text }}
                    </p>
                    <small class="float-right">
                        (by {{ $commentator->name }})
                    </small>
                </li>
            @endforeach
        </ul>
    </div>
@endif