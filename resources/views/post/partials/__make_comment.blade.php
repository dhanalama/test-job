@auth
    @include('errors.validation')

    <h5>Write your comment:</h5>
    <form action="{{ route('post.comment', ['id' => $postDetail->id]) }}" method="post">
        {!! csrf_field() !!}
        <textarea name="comment"
                  id="comment"
                  rows="3"
                  class="form-control"
                  required
        ></textarea>
        <div class="clearfix">
            <button class="btn btn-sm float-right btn-primary m-1">Submit</button>
        </div>
    </form>
@endauth