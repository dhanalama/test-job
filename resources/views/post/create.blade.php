@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card mt-3">
                    <div class="card-header">Create Post</div>
                    <div class="card-body">

                        @include('errors.validation')

                        <form action="{{ route('post.store') }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group row">
                                <label for="title" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text"
                                           name="title"
                                           class="form-control"
                                           id="title"
                                           value="{{ old('title') ?: '' }}"
                                           placeholder="Enter Title"
                                    >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea type="text"
                                              name="description"
                                              class="form-control"
                                              id="description"
                                              placeholder="Enter Description"
                                    >{{ old('description') ?: '' }}</textarea>
                                </div>
                            </div>
                            <div class="clearfix">
                                <button class="btn float-right btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
