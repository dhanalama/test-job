@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @include('flash::message')

                <div class="card m-t-4">
                    <div class="card-body">

                        <div class="clearfix">
                            <h2 class="float-left">{{ $postDetail->title }}
                                <small class="fs-13">by {{ $postDetail->author }}</small>
                            </h2>
                            <span class="float-right">{{ $postDetail->created_at->diffForHumans() }}</span>
                        </div>
                        <p>{{ $postDetail->description }}</p>

                        <br/>
                        <br/>

                        @include('post.partials.__make_comment')

                        @include('post.partials.__comments')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
