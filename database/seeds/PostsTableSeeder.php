<?php


class PostsTableSeeder extends ParentTableSeeder
{
	protected $tables = ['posts'];
	
	public function __construct()
	{
		parent::__construct();
	}
	
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(\App\Models\Post::class, 10)->create();
    }
}
