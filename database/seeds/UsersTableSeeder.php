<?php

use App\User;

class UsersTableSeeder extends ParentTableSeeder
{
	protected $tables = ['users'];
	

	public function __construct()
	{
		parent::__construct();
	}
	
	
	/**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(User::class, 10)->create();
    }
}
