<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParentTableSeeder extends Seeder
{
 	protected $tables = [];
	
 	public function __construct()
	{
		$this->truncateTables();
	}
	
	
	protected function truncateTables()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		foreach ($this->tables as $table) {
			DB::table($table)->truncate();
		}
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}
}
