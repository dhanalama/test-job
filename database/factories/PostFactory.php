<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Post::class, function (Faker $faker) {
	$title = $faker->words(2, true);
	
    return [
        'user_id' 		=> function() {
    		if (\App\User::count() > 3) {
    			return (\App\User::all()->random())->id;
			}
    		return (factory(\App\User::class)->create())->id;
		},
		'title' 		=> $title,
		'slug'			=> $title,
		'description' 	=> $faker->sentences(4, true)
    ];
});
